﻿using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using MyProject;
using MyProject.Service;
using Moq;

namespace MyProjectTests.Service
{
    public class ResultServiceTests
    {
        [Fact()]
        //public void SetRecentOverweightResult_ForOverweightResult_UpdatesProperty()
        //{
        //    //arrange
        //    var result = new BmiResult { BmiClassification = BmiClassification.Overweight };
        //    var resultService = new ResultService();

        //    //act
        //    resultService.SetRecentOverweightResult(result);

        //    //assert
        //    resultService.RecentOverweightResult.Should().Be(result);
        //}
        public void SetRecentOverweightResult_ForOverweightResult_DoesNotUpdateProperty()
        {
            //arrange
            var result = new BmiResult { BmiClassification = BmiClassification.Obesity };
            //var resultService = new ResultService();
            var resultService = new ResultService(new ResultRepository()); //mozna by zamiast tego, co w nawiasie, utworzyc mock

            //act
            resultService.SetRecentOverweightResult(result);

            //assert
            resultService.RecentOverweightResult.Should().BeNull();
        }

        [Fact()]
//ZLE, NIE DZIALA
        public async Task SaveUnderweightResultAsync_ForUnderweightResult_InvokesSaveResultAsync()
        {
            var result = new BmiResult { BmiClassification = BmiClassification.Underweight };
            var repoMock = new Mock<IResultRepository>();
            var resultService = new ResultService(repoMock.Object);

            //act
            await resultService.SaveUnderweightResultAsync(result);

            //assert
            repoMock.Verify(mock => mock.SaveResultAsync(result), Times.Once);
        }
        
        [Fact]
//TO TEZ TAK NAPRAWDE NIE DZIALA
        public async Task SaveUnderweightResultAsync_ForNonUnderweightResult_DoesntInvokeSaveResultAsync()
        {
            // arrange
            var result = new BmiResult { BmiClassification = BmiClassification.Normal };
            var repoMock = new Mock<IResultRepository>();

            var resultService = new ResultService(repoMock.Object);
            // act
            await resultService.SaveUnderweightResultAsync(result);

            // assert
            repoMock.Verify(mock => mock.SaveResultAsync(result), Times.Never);
        }
    }
}

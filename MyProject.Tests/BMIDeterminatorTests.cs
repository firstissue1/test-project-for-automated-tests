﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProject.Tests
{
    public class BmiDeterminatorTests
    {
        [Theory] //teoria (zamiast fact) pozwala zaciagnac input liczbowy do funkcji, i zezwala na kilka scenariuszy osobnych - wywoluja sie z automatu
        [InlineData(10.0, BmiClassification.Underweight) ]
        [InlineData(17.0, BmiClassification.Underweight)]
        [InlineData(18.0, BmiClassification.Underweight)]
        [InlineData(8.0, BmiClassification.Underweight)]
        [InlineData(18.9, BmiClassification.Normal)]
        [InlineData(21.0, BmiClassification.Normal)]
        [InlineData(24.8, BmiClassification.Normal)]
        [InlineData(25.8, BmiClassification.Overweight)]
        [InlineData(28.8, BmiClassification.Overweight)]
        [InlineData(30.8, BmiClassification.Obesity)]
        [InlineData(34.8, BmiClassification.Obesity)]
        [InlineData(34.9, BmiClassification.Obesity)]
        [InlineData(35.9, BmiClassification.ExtremeObesity)]
        [InlineData(55.9, BmiClassification.ExtremeObesity)]
        public void DetermineBmi_ForGivenBmi_ReturnsCorrectClassification(double bmi, BmiClassification classification)
        {
            BmiDeterminator bmiDeterminator = new BmiDeterminator();

            BmiClassification result = bmiDeterminator.DetermineBmi(bmi);

            Assert.Equal(classification, result);

            //[Fact]
            //public void DetermineBmi_ForBmiBelow18_5_ReturnsUnderweightClassification()
            //{
            //arrange
            //BmiDeterminator bmiDeterminator = new BmiDeterminator();
            //double bmi = 10;
            ////act
            //BmiClassification result = bmiDeterminator.DetermineBmi(bmi);
            ////assert
            //Assert.Equal(BmiClassification.Underweight, result);

            //bad practice below
            //BmiDeterminator bmiDeterminator = new BmiDeterminator();
            //double[] bmis = new double[] { 5, 10, 15, 18 }; 
            //foreach (var bmi in bmis)
            //{
            //    BmiClassification result = bmiDeterminator.DetermineBmi(bmi);
            //    Assert.Equal(BmiClassification.Underweight, result);
            //}

        }
    }
}

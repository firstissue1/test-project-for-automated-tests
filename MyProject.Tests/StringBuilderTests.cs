using System.Text;

namespace MyProject.Tests
{
    public class StringBuilderTests //good practice: NazwaTestowanegoCzegos + Tests
    {
        [Fact]
        public void Append_ForTwoStrings_ReturnsConcatenatedString() //good practice: NazwaMetody_Scenariusz_Rezultat
        {
            // arrange - 1. czesc testu (inicjalizujemy potrzebne obiekty)
            StringBuilder sb = new StringBuilder();

            // act - 2. czesc testu (tu wykonuje sie dana funkcjonalnosc)
            sb.Append("test")
                .Append("test2");

            string result = sb.ToString();
 
            // assert - 3. czesc testu (sprawdzenie testu, rezultat)

            Assert.Equal("testtest2", result); //good practice: 1 assercja per 1 scenariusz testowy, bo w wyniku, jesli jakakolwiek asercja sie nie powiedzie, to caly test jest na czerwono
        }
    }
}
﻿using System.Threading.Tasks;

namespace MyProject.Service
{
    public interface IResultRepository
    {
        Task SaveResultAsync(BmiResult result);
    }
}
﻿namespace MyProject
{
    public interface IBmiDeterminator //ten i-face jest potrzebny, aby moc wstrzyknac jego dowolna implementacje do fasady
    {
        BmiClassification DetermineBmi(double bmi);
    }
}
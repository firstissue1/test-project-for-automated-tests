﻿using Xunit;
using Exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;

namespace Exercise.Tests
{
//    [TestClass()]
    public class StringHelperTests
    {
        private const string F_EXAMPLE = "pszczola miodna";
        private const string S_EXAMPLE = "krowa pasie sie na lace";
        private const string T_EXAMPLE = "1 pszczola nie lata, a 2 lataja";

        private const string OF_EXAMPLE = "miodna pszczola";
        private const string OS_EXAMPLE = "lace na sie pasie krowa";
        private const string OT_EXAMPLE = "lataja 2 a lata, nie pszczola 1";

        //[Theory]
        //[InlineData(F_EXAMPLE, OF_EXAMPLE)]
        //[InlineData(S_EXAMPLE, OS_EXAMPLE)]
        //[InlineData(T_EXAMPLE, OT_EXAMPLE)]
        [Fact]
        public void ReverseWords_ForGivenSentence_ReturnsReversedSentence(/*string wordInput, string result*/)
        {
            string wordInput = "ala ma kota";

            string wordsToCheck = StringHelper.ReverseWords(wordInput);

            wordsToCheck.Should().Be("kota ma ala");
        }

        [Fact]
        public void IsPalindromeTest()
        {
            Assert.Fail();
        }
    }
}